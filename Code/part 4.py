import time
from datetime import datetime
import numpy as np
from os.path import exists
import pandas as pd
import random
import os



client_random = np.random.randint(100000, 999999, size=(36500, 1))
df = pd.DataFrame(client_random, columns=['Client_ID'])
types = ['Healthy', 'Retirement', 'Business', 'Onetime']
df['type'] = random.choices(types, weights=[0.11,0.16,0.21,0.51], k= 36500)
df['type'].tolist()
print(df.head())
df.to_csv(os.path.abspath("../Results/DF/df_part4.csv"))


input4 = "../Results/DF/df_part4.csv"
df = pd.read_csv(os.path.abspath(input4), sep = "," )
new_client = int(input('give me the ID of the client'))
new_type = str(input('what is this client type'))
client_id = df['Client_ID'].values
for client in client_id :
    if input == client :
        print ('client already exists ')
    else :
        df['Client_ID'] = client_id
        df['type'] = new_type



first_course = ["Soup", "Tomato-Mozarella", "Oysters"]
second_course = ["Salad", "Spaghetti", "Steak", "Lobster"]
third_course = ["Ice cream", "Pie"]
course_one = 0
course_two = 0
course_three = 0
def GoToRestaurant (self) :
    #first course
    print (first_course)
    input ('chose the of first course from 0 to 2  ')
    if int(input) == 0 :
        self.course1 = self.first_course[0]
        print("You have chosen " + self.course1)
    elif int(input) == 1:
        self.course1 = self.first_course[1]
        print("You have chosen " + self.course1)
    elif int(input) == 2:
        self.course1 = self.first_course[2]
        print("You have chosen " + self.course1)
    else :
        print('try again')
    print (second_course)

    #second course
    input('chose your second course from 0 to 3 ')
    if int(input) == 0 :
        self.course1 = self.second_course[0]
        print("You have chosen " + self.course2)
    elif int(input) == 1:
        self.course1 = self.second_course[1]
        print("You have chosen " + self.course2)
    elif int(input) == 2:
        self.course1 = self.second_course[2]
        print("You have chosen " + self.course2)
    elif int(input) == 3:
        self.course1 = self.second_course[3]
        print("You have chosen " + self.course2)
    else :
        print('try again')

    #third course
    input('chose your third course 0 or 1 ')
    if int(input) == 0:
        self.course3 = self.third_course[0]
        print("You have chosen " + self.course3)
    elif int(input) == 1:
        self.course3 = self.third_course[1]
        print("You have chosen " + self.course3)
    else :
        print ('try again')

    # adding times stamp
    timestamp = time.time()
    date_time = datetime.fromtimestamp(timestamp)
    string_date = date_time.strftime("%d-%m-%Y, %H:%M:%S")
    new_data = Add_columns(self.course1, self.course2, self.course3, string_date, self.id)
    new_data.df_new.to_csv("../Results/DF/df_part4.csv")


#Class of new clients
class Add_columns :
    first_order = []
    second_order = []
    third_order = []
    client_id = []
    order_time = []
    final_df = pd.DataFrame()
    def __init__(self, first,second,third,time,new_id):
        self.first_order.append(first)
        self.second_order.append(second)
        self.third_order.append(third)
        self.order_time.append(time)
        self.client_id.append(new_id)
        self.updateDataFrame()

    def NewDataFrame(self):
        df_id = np.array(self.client_id, dtype='object')
        self.df_final['Client_ID'] = df_id.tolist()

        df_course1 = np.array(self.first_order, dtype='object')
        self.df_final["first course"] = df_course1.tolist()

        df_course2 = np.array(self.second_order, dtype='object')
        self.df_final["second course"] = df_course2.tolist()

        df_course3 = np.array(self.third_order, dtype='object')
        self.df_final["third course"] = df_course3.tolist()

        df_time = np.array(self.order_time, dtype='object')
        self.df_final["time"] = df_time.tolist()


# creating the file that contains  all the new information for 5 years and 20 courses per day

