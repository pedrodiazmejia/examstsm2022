
import pandas as pd
import numpy as np
import os
import matplotlib.pyplot as plt
import scipy.stats

#another way to doit
#import the data
"""input1 = "../Data/part1.csv"

df_first = pd.read_csv(os.path.abspath(input1),sep = "," )
print(df_first.describe())

names = ["FIRST_COURSE","SECOND_COURSE","THIRD_COURSE"]
#First issue founded, it was hard to determine if the count of the histogram should be in % or in counts in the same set. we decide to go with
#the counting, but the change could be donde in the 19 line, with density=TRUE
for x in names:

    fig, ax = plt.subplots()
    n, bins, patches = ax.hist(df_first[f"{x}"], 10)
    y = scipy.stats.norm.pdf(bins, 0, 1)
    ax.set_xlabel("Cost of the course")
    ax.set_ylabel("Number of times present")
    ax.set_title(f"distribution of {x}")
    plt.savefig(os.path.abspath(f"../Results/Graphs/1_{x}.png"))
    plt.show()


print("---------------------------------------------------------------------------------------------------")"""
#Total Consumption
"""df_first["Total_consumption"] = df_first["FIRST_COURSE"] + df_first["SECOND_COURSE"] + df_first["THIRD_COURSE"]



#firstcourse drinks price:
conditions1 = [
    (df_first["FIRST_COURSE"] == 0),
    (df_first["FIRST_COURSE"] < 15),
    (df_first["FIRST_COURSE"] < 20),
    (df_first["FIRST_COURSE"] > 20),
]
choices1 = [
    (0),
    (df_first["FIRST_COURSE"]- 3),
    (df_first["FIRST_COURSE"]- 15),
    (df_first["FIRST_COURSE"] - 20),
]
df_first["D_STARTERS"] = np.select(conditions1, choices1, default=0)
#Secondcourse drinks price:
conditions2 = [
    (df_first["SECOND_COURSE"] == 0),
    (df_first["SECOND_COURSE"] < 20),
    (df_first["SECOND_COURSE"] < 25),
    (df_first["SECOND_COURSE"] < 40),
    (df_first["SECOND_COURSE"] > 40),
]
choices2 = [
    (0),
    (df_first["SECOND_COURSE"]- 9),
    (df_first["SECOND_COURSE"]- 20),
    (df_first["SECOND_COURSE"] - 25),
    (df_first["SECOND_COURSE"] - 40),
]
df_first["D_MAINS"] = np.select(conditions2, choices2, default=0)

#Thirdcourse drinks price:
conditions3 = [
    (df_first["THIRD_COURSE"] == 0),
    (df_first["THIRD_COURSE"] < 15),
    (df_first["THIRD_COURSE"] >15),

]
choices3 = [
    (0),
    (df_first["THIRD_COURSE"]- 10),
    (df_first["THIRD_COURSE"]- 15),
]
df_first["D_DESSERTS"] = np.select(conditions3, choices3, default=0)

#What they ordered in the first one
conditions4 = [
    (df_first["FIRST_COURSE"] == 0),
    (df_first["FIRST_COURSE"] < 15),
    (df_first["FIRST_COURSE"] < 20),
    (df_first["FIRST_COURSE"] > 20),
]
choices4 = [
    ("N/A"),
    ("Soup"),
    ("Tomato-Mozarella"),
    ("Oysters"),
]
df_first["M_STARTERS"] = np.select(conditions4, choices4, default=0)

#What they ordered in the second
conditions5 = [
    (df_first["SECOND_COURSE"] == 0),
    (df_first["SECOND_COURSE"] < 20),
    (df_first["SECOND_COURSE"] < 25),
    (df_first["SECOND_COURSE"] < 40),
    (df_first["SECOND_COURSE"] > 40),
]
choices5 = [
    ("N/A"),
    ("Salad"),
    ("Spaghetti"),
    ("Steak"),
    ("Lobster"),
]
df_first["M_MAINS"] = np.select(conditions5, choices5, default=0)

#What they ordered in the third
conditions6 = [
    (df_first["THIRD_COURSE"] == 0),
    (df_first["THIRD_COURSE"] < 15),
    (df_first["THIRD_COURSE"] >15),
]
choices6 = [
    ("N/A"),
    ("Pie"),
    ("Ice-Cream"),
]
df_first["M_DESSERTS"] = np.select(conditions6, choices6, default=0)

print(df_first.describe())
print(df_first.head(15))

#df_first.to_csv(os.path.abspath("../Results/DF/df_first.csv"))

#another way of doint it"""

input1 = "../Data/part1.csv"

df_first = pd.read_csv(os.path.abspath(input1),sep = "," )

course_one = df_first['FIRST_COURSE'].values
#print(course_one)
for course in course_one:
    if course ==0 :
        df_first['cost_of_first_meal'] = 0
        df_first['cost_of_first_drink'] = 0
    elif course > 3 :
        df_first['cost_of_first_meal'] = 3
        df_first['cost_of_first_drink'] = (course-3)
    elif course> 15 :
        df_first['cost_of_first_meal'] = 15
        df_first['cost_of_first_drink'] = (course-15)
    else:
        df_first['cost_of_first_meal'] = 20
        df_first['cost_of_first_drink'] = (course - 20)

course_two = df_first['SECOND_COURSE'].values

for course in course_two:
    if course ==0 :
        df_first['cost_of_second_meal'] = 0
        df_first['cost_of_second_drink'] = 0
    elif  course > 9 :
        df_first['cost_of_second_meal'] = 9
        df_first['cost_of_second_drink'] = (course-9)
    elif course > 15 :
        df_first['cost_of_second_meal'] = 20
        df_first['cost_of_second_drink'] = (course-20)
    elif course > 25 :
        df_first['cost_of_second_meal'] = 25
        df_first['cost_of_second_drink'] = (course - 25)
    else:
        df_first['cost_of_second_meal'] = 40
        df_first['cost_of_second_drink'] = (course - 40)

desserts = df_first['THIRD_COURSE'].values
for desert in desserts:
    if desert ==0 :
        df_first['cost_of_third_meal'] = 0
        df_first['cost_of_third_drink'] = 0
    elif desert > 10 :
        df_first['cost_of_third_meal'] = 10
        df_first['cost_of_third_drink'] = (desert-10)
    elif desert > 15 :
        df_first['cost_of_third_dessert'] = 15
        df_first['cost_of_third_drink'] = (desert-15)





df_first.to_csv(os.path.abspath("../Results/DF/df_added_columns.csv"))