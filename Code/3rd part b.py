import pandas as pd
import os
import numpy as np
import matplotlib.pyplot as plt


#distribution of dishes per course

input1 = "../Results/DF/df_first.csv"
input2 = "../Results/DF/df_clustered.csv"
third_df = pd.read_csv(os.path.abspath(input1),sep = "," )
forth_df = pd.read_csv(os.path.abspath(input2),sep = "," )

distribution_starters = third_df['M_STARTERS'].value_counts()
distribution_mains = third_df['M_MAINS'].value_counts()
distribution_desserts = third_df['M_DESSERTS'].value_counts()
print (distribution_starters, distribution_mains , distribution_desserts)

# distribution of dishes per customer type
#first step is to merge the columns with clusters with the df_first data frame
MyDataframe= pd.merge(third_df, forth_df,
                        how='outer', on='CLIENT_ID')


grouped = MyDataframe.groupby(['CLUSTER','M_STARTERS']).count()
print(grouped)

#distribution of the cost of the drinks per cost
print(third_df.hist(column="D_STARTERS"))
plt.show()
print(third_df.hist(column="D_MAINS"))
plt.show()
print(third_df.hist(column="D_DESSERTS"))
plt.show()

