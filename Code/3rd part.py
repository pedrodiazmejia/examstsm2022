import numpy as np
import pandas as pd
import os
import matplotlib.pyplot as plt

input1 = "../Results/DF/df_labeled.csv"
df = pd.read_csv(os.path.abspath(input1), sep = "," )
client = ["Retirement", "Business", "Onetime","Healthy"]
print(client)

print()
x= input("Client name?")
x = x.lower()
if  x=="retirement":
    df1 = df[df["CLUSTER"] == 0]
    print(f"the total number of {x} customers is {len(df1)/len(df)*100}%")
elif x=="business":
    df1 = df[df["CLUSTER"] == 1]
    print(f"the total number of {x} customers is {len(df1) / len(df)*100}%")
elif x=="onetime":
    df1 = df[df["CLUSTER"] == 2]
    print(f"the total number of {x} customers is {len(df1) / len(df)*100}%")
elif x=="healthy":
    df1 = df[df["CLUSTER"] == 3]
    print(f"the total number of {x} customers is {len(df1) / len(df)*100}%")
else:
    print(f"Please choose between: {client}" )


print("_--------------------------------------------------------------------------------------")

y = input("Likely for customer to order a course;please give the number ")
y = int(y)


course = [1, 2, 3]
client = ["Retirement", "Business", "Onetime","Healthy"]


if y == 1:

    dfa = df[(df["CLUSTER"] == 0)]
    df1 = df[(df["CLUSTER"] == 0) & (df["FIRST_COURSE"] > 0)]
    print(f"{client[0]} customers are more likely to order course #{y}  times {int(len(df1)/len(dfa)*100)}%" )

    dfb = df[(df["CLUSTER"] == 1)]
    df2 = df[(df["CLUSTER"] == 1) & (df["FIRST_COURSE"] > 0)]
    print(f"{client[1]} customers are more likely to order course # {y} {int(len(df2)/len(dfb)*100)}% of the times" )


    dfc = df[(df["CLUSTER"] == 2)]
    df3 = df[(df["CLUSTER"] == 2) & (df["FIRST_COURSE"] > 0)]
    print(f"{client[2]} customers are more likely to order course # {y}  times {int(len(df2)/len(dfc)*100)}% of the times" )

    dfd = df[(df["CLUSTER"] == 3)]
    df4 = df[(df["CLUSTER"] == 3) & (df["FIRST_COURSE"] > 0)]
    print(f"{client[3]} customers are more likely to order course # {y}  times {int(len(df4)/len(dfd)*100)}% of the times" )

elif y == 2:

    dfa = df[(df["CLUSTER"] == 0)]
    df1 = df[(df["CLUSTER"] == 0) & (df["SECOND_COURSE"] > 0)]
    print(f"{client[0]} customers are more likely to order course #{y}  times {int(len(df1)/len(dfa)*100)}% of the times" )

    dfb = df[(df["CLUSTER"] == 1)]
    df2 = df[(df["CLUSTER"] == 1) & (df["SECOND_COURSE"] > 0)]
    print(f"{client[1]} customers are more likely to order course # {y}   times {int(len(df2)/len(dfb)*100)}% of the times" )


    dfc = df[(df["CLUSTER"] == 2)]
    df3 = df[(df["CLUSTER"] == 2) & (df["SECOND_COURSE"] > 0)]
    print(f"{client[2]} customers are more likely to order course # {y}  times {int(len(df2)/len(dfc)*100)}% of the times" )

    dfd = df[(df["CLUSTER"] == 3)]
    df4 = df[(df["CLUSTER"] == 3) & (df["SECOND_COURSE"] > 0)]
    print(f"{client[3]} customers are more likely to order course # {y}  times {int(len(df4)/len(dfd)*100)}% of the times" )

elif y == 3:

    dfa = df[(df["CLUSTER"] == 0)]
    df1 = df[(df["CLUSTER"] == 0) & (df["THIRD_COURSE"] > 0)]
    print(f"{client[0]} customers are more likely to order course # {y}  times {int(len(df1)/len(dfa)*100)}% of the times" )

    dfb = df[(df["CLUSTER"] == 1)]
    df2 = df[(df["CLUSTER"] == 1) & (df["THIRD_COURSE"] > 0)]
    print(f"{client[1]} customers are more likely to order course # {y}   times {int(len(df2)/len(dfb)*100)}% of the times" )


    dfc = df[(df["CLUSTER"] == 2)]
    df3 = df[(df["CLUSTER"] == 2) & (df["THIRD_COURSE"] > 0)]
    print(f"{client[2]} customers are more likely to order course # {y}  times {int(len(df2)/len(dfc)*100)}% of the times" )

    dfd = df[(df["CLUSTER"] == 3)]
    df4 = df[(df["CLUSTER"] == 3) & (df["THIRD_COURSE"] > 0)]
    print(f"{client[3]} customers are more likely to order course # {y}  times {int(len(df4)/len(dfd)*100)}% of the times" )


print ("---------------------------------------------------------------------------------------------------")
number = [0,1,2,3]
starter = ["Soup", "Tomato-Mozarella", "Oysters"]
main = ["Salad","Spaghetti","Steak","Lobster"]
dess = ["Pie","Ice-Cream"]
courses = ["FIRST_COURSE","SECOND_COURSE","THIRD_COURSE"]
time = ["M_STARTERS","M_MAINS","M_DESSERTS"]


data = {"CLIENT_LABEL":"TEST1","STARTER":"DISH_TEST","ORDER%":[0.5]}
df_prob = pd.DataFrame(data)
print(df_prob)

i = 0

while i < 4:
        for p in starter :
            dfj = df[(df["CLUSTER"] == i) & (df["FIRST_COURSE"] != 0)]
            dfa = df[(df["CLUSTER"] == i) & (df["M_STARTERS"] == f"{p}")]
            data1 = {"CLIENT_LABEL":f"{client[i]}","STARTER":f"{p}","ORDER%":(len(dfa)/len(dfj)*100)}
            df_prob = df_prob.append(data1, ignore_index=True)


        i = i + 1
print(df_prob)
i = 0
while i < 4:
     for p in main:
            dfj = df[(df["CLUSTER"] == i) & (df["SECOND_COURSE"] != 0)]
            dfa = df[(df["CLUSTER"] == i) & (df["M_MAINS"] == f"{p}")]
            data1 = {"CLIENT_LABEL": f"{client[i]}", "STARTER": f"{p}", "ORDER%": (len(dfa) / len(dfj) * 100)}
            df_prob = df_prob.append(data1, ignore_index=True)
     i = i + 1

i = 0
while i < 4:
     for p in dess:
            dfj = df[(df["CLUSTER"] == i) & (df["THIRD_COURSE"] != 0)]
            dfa = df[(df["CLUSTER"] == i) & (df["M_DESSERTS"] == f"{p}")]
            data1 = {"CLIENT_LABEL": f"{client[i]}", "STARTER": f"{p}", "ORDER%": (len(dfa) / len(dfj) * 100)}
            df_prob = df_prob.append(data1, ignore_index=True)

     i = i + 1


print(df_prob)

df_prob.to_csv(os.path.abspath("../Results/DF/df_orders.csv"))

