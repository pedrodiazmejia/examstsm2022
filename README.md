the read me file <div id="top"></div>
<!--
*** Thanks for checking out the Best-README-Template. If you have a suggestion
*** that would make this better, please fork the repo and create a pull request
*** or simply open an issue with the tag "enhancement".
*** Don't forget to give the project a star!
*** Thanks again! Now go create something AMAZING! :D
-->



<!-- PROJECT SHIELDS -->
<!--
*** I'm using markdown "reference style" links for readability.
*** Reference links are enclosed in brackets [ ] instead of parentheses ( ).
*** See the bottom of this document for the declaration of the reference variables
*** for contributors-url, forks-url, etc. This is an optional, concise syntax you may use.
*** https://www.markdownguide.org/basic-syntax/#reference-style-links
-->


<!-- Final_Exam_python -->
<br />
<div align="center">
  <a href="https://github.com/github_username/repo_name">
  </a>

<h3 align="center">Final_Exam_python</h3>

  <p align="center">
    project_description
    <br />
    <a href="https://github.com/github_username/repo_name"><strong>Explore the docs »</strong></a>
    <br />
    <br />
    <a href="https://https://tsm-education.fr/">TSM website</a>
    ·
    <a href="mailto:pedro.diaz-mejia@tsm-education.fr">Report Bug to Pedro</a>
    ·
    <a href="mailto:malek.hamedi@tsm-education.fr ">Report Bug to Malek</a>
  </p>
</div>



<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
      </ul>
    </li>
    <li><a href="#usage">Usage</a></li>
    <li><a href="#roadmap">Roadmap</a></li>
    <li><a href="#contact">Contact</a></li>

  </ol>
</details>



<!-- ABOUT THE PROJECT -->
## About The Project


The main idea of this project is to analyse the data coming from a random restaurant. We want to classify it with different kinds of clustering and obtain an analyse. Finally we recreate the data aiming to proove distributions. `Malek Hamedi`, `Pedro Diaz`, `Repo:Examstsm`,

<p align="right">(<a href="#top">back to top</a>)</p>



### Built With

* [python](https://www.python.org/)

<p align="right">(<a href="#top">back to top</a>)</p>



<!-- GETTING STARTED -->
## Getting Started

First, run the 1st part. It will give you basic examples and graphs about the distribution of each course. Also it will help you to obtain the random number created by the drinks in each course.

### Prerequisites

This is an example of how to list things you need to use the software and how to install them.
* pandas
  ```sh
  pandas install
  ```
* Numpy
  ```sh
  numpy install
  ```
* os
  ```sh
  os install
  ```
* matplotlib.pyplot
    ```sh
  matplotlib install
  ```
* Sklearn
    ```sh
  sklearn install
  kmeans install
  ```







<!-- USAGE EXAMPLES -->
## Usage

Use this space to show useful examples of how a project can be used. Additional screenshots, code examples and demos work well in this space. You may also link to more resources.

_For more examples, please refer to the [Documentation](https://example.com)_

<p align="right">(<a href="#top">back to top</a>)</p>



<!-- ROADMAP -->
## Roadmap

- [ ] Part 1
- [ ] Part 2
- [ ] Part 3
- [ ] Part 4
    - [ ] Recreating data
    
<p align="right">(<a href="#top">back to top</a>)</p>





<!-- CONTACT -->
## Contact

Malek Hamedy - [@Malek-Hamedy](https://.com/) -malek.hamedi@tsm-education.fr
Pedro Díaz - [@Pedro-Diaz](https://.com/) -pedro.diaz-mejia@tsm-education.fr

<p align="right">(<a href="#top">back to top</a>)</p>





<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
[contributors-shield]: https://img.shields.io/github/contributors/github_username/repo_name.svg?style=for-the-badge
[contributors-url]: https://github.com/github_username/repo_name/graphs/contributors
[forks-shield]: https://img.shields.io/github/forks/github_username/repo_name.svg?style=for-the-badge
[forks-url]: https://github.com/github_username/repo_name/network/members
[stars-shield]: https://img.shields.io/github/stars/github_username/repo_name.svg?style=for-the-badge
[stars-url]: https://github.com/github_username/repo_name/stargazers
[issues-shield]: https://img.shields.io/github/issues/github_username/repo_name.svg?style=for-the-badge
[issues-url]: https://github.com/github_username/repo_name/issues
[license-shield]: https://img.shields.io/github/license/github_username/repo_name.svg?style=for-the-badge
[license-url]: https://github.com/github_username/repo_name/blob/master/LICENSE.txt
[linkedin-shield]: https://img.shields.io/badge/-LinkedIn-black.svg?style=for-the-badge&logo=linkedin&colorB=555
[linkedin-url]: https://linkedin.com/in/linkedin_username
[product-screenshot]: images/screenshot.png